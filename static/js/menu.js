$(window).scroll(function() {
  var nonDarkPages = /index$|index\.html$|es\/$|en\/$/;
  if (nonDarkPages.test(window.location.pathname)) {
    if($(window).scrollTop() > 250){
      /*$('#elephant').css({'height': '3.5vh'});*/
      $("header").addClass("dark");
    }else{
      /*$('#elephant').css({'height': '20vh'});*/
      $("header").removeClass("dark");
    }
    if ($(window).scrollTop() > 150) {
      $(".mobile-menu-button-container").addClass("dark");
      $("#mobile-menu").addClass("dark");
    } else {
      $(".mobile-menu-button-container").removeClass("dark");
      $("#mobile-menu").removeClass("dark");
    }
  }/* else {
    if($(window).scrollTop() > 100){
      $('#elephant').css({'height': '3.5vh'});
    }else{
      $('#elephant').css({'height': '20vh'});
    }
  }*/
});

function deployMobileMenu(lang) {
  var menu = $("#mobile-menu");
  var menuButton = $("#mobile-menu-button");
  if (menu.hasClass("open")) {
    menu.removeClass("open");
    menuButton.removeClass("open");
  } else {
    menu.addClass("open");
    menuButton.addClass("open");
  }
}
function toggleSubmenu(elmToToggle) {
  if ($(elmToToggle).is(":visible")) {
    $(elmToToggle).hide();
  } else {
    $(elmToToggle).show();
  }
}
function toggleMobileSubmenu(elmToToggle) {
  arrow = $(elmToToggle).parent().find(".submenu-arrow");
  if ($(elmToToggle).hasClass("open")) {
    $(elmToToggle).removeClass("open");
    arrow.removeClass("up");
  } else {
    $(elmToToggle).addClass("open");
    arrow.addClass("up");
  }
}
function calculateOneMenu(items, parent, submenuParent, cssClass, lang, mobile) {
  items.forEach(function(btn){
    if (btn.link != undefined) {
      parent.append('<div class="menu-button ' + cssClass + '"><a href="' + btn.link + '">' + btn.name + '</a></div>');
      return;
    }
    if (btn.submenu != undefined) {

      if (mobile) {
        var length = $(".mobile-submenu").length;
        parent.append('<div class="menu-button mobile-submenu mobile-submenu-' + length + '"><a href="#" onclick="toggleMobileSubmenu(\'.mobile-submenu.mobile-submenu-'
          + length + ' .mobile-submenu-buttons\');">' + btn.name + '<img src="../assets/white-arrow.svg" class ="submenu-arrow"></a><div class="mobile-submenu-buttons"></div></div>');
        calculateOneMenu(btn.submenu, $(".mobile-submenu.mobile-submenu-" + length + " .mobile-submenu-buttons"), submenuParent, 'submenu-button', lang, true);
      } else {
        var length = $(submenuParent).children().length;
        parent.append('<div class="menu-button"><a href="#" onclick="toggleSubmenu(\'.submenu.submenu-' + length + '\');">' + btn.name + '</a></div>');
        $(submenuParent).append('<div class="submenu submenu-' + length + '"><div class="menu-buttons"></div></div>');
        calculateOneMenu(btn.submenu, $(submenuParent).find(".submenu-" + length + " .menu-buttons"), submenuParent, 'submenu-button', lang, false);
      }
    }
  });
}

function calculateMenu(messages, lang) {
  $("body").append('<div class="mobile-menu-button-container"><div id="mobile-menu-button" onclick="deployMobileMenu()"></div></div><div class="" id="mobile-menu"><div class="mobile-menu-buttons"></div></div>');
  var nonDarkPages = /index$|index\.html$|es\/$|en\/$/;
  if (!nonDarkPages.test(window.location.pathname)) {
    $("header").addClass("dark");
    $(".mobile-menu-button-container").addClass("dark");
    $("#mobile-menu").addClass("dark");
  }
  $(".mobile-menu-button-container").append('<a href="index"><img src="../assets/elephant.svg" alt="" id="mobile-elephant-left"/></a>');
  $(".mobile-menu-button-container").append('<a href="index"><img src="../assets/banner-logo.svg" class="mobile-header-logo"/></a>');

  calculateOneMenu(messages.menu, $("#menu"), "#submenu-container", '', lang, false);
  calculateOneMenu(messages.menu, $("#mobile-menu .mobile-menu-buttons"), "#submenu-container", '', lang, true);

  nextLang = lang == 'es' ? 'en' : 'es';

  $("#menu").append('<div class="menu-button lang-menu-button" onclick="toggleLang(\'' + lang + '\',\'' + nextLang + '\')"><strong>' + lang.toUpperCase()
    + '</strong>/' + nextLang.toUpperCase() + '</div>');
    $("#mobile-menu .mobile-menu-buttons").append('<div class="menu-button lang-menu-button" onclick="toggleLang(\'' + lang + '\',\'' + nextLang + '\')"><strong>' + lang.toUpperCase()
      + '</strong>/' + nextLang.toUpperCase() + '</div>');
    processBottomBanner(window.location.pathname.substring(1), lang);
}

function toggleLang(lang, next) {
  var href = $("link[hreflang|='"+ next +"']")[0].href;
  window.location.href = href;
}

function processBottomBannerAsSubscribe(page, lang) {
}

function processBottomBannerAsContact(page, lang) {
  var hrefcontact = lang === 'es' ? 'contacto' : 'contact';
  $(".subscribe-banner").append('<div class="text" id="have-doubts"></div><a class="subscribe-banner-button" id="letsTalk" href="'+hrefcontact+'"></a>');
}

function processBottomBanner(page, lang) {
  var noBannerPages = /contact$|contact\.html$|contacto$|contacto\.html$/;
  var subscribePages = [];
  if (subscribePages.includes(page)) {
    processBottomBannerAsSubscribe(page, lang);
  } else if (!noBannerPages.test(page)) {
    processBottomBannerAsContact(page, lang);
  }
}
