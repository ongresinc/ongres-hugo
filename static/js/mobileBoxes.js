function createBoxes(elements, selected) {
  if (selected == undefined) {
    selected = 0;
  }
  return '<div class="mobile-boxes">' +
  elements.map((element, index) => '<div class="mobile-box'+ (index == selected ? ' open' : '') + '" onclick="openBox(this)"><div class="mobile-box-title">'
   + element.title + '<img src="../assets/orange-arrow.svg" class="mobile-box-toggler"></div><div class="mobile-box-desc">' + element.description + '</div></div>')
  .join('') +
  '</div>';
}

function openBox(e) {
  console.log(e);
  if ($(e).hasClass("open")) {
    $(e).removeClass("open");
  } else {
    $(e).addClass("open");
  }
  //var boxToOpen = $(".mobile-boxes .mobile-box:nth-child(" + selected + ")");
}
