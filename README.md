
# OnGres-Hugo

This repository contains the web project for the blog of OnGres based on [Hugo](https://gohugo.io/) - The world’s fastest framework for building websites.

Hugo is a static site generator with focus on speed, writen in Go.

The main idea is that you create your content using Markdown and Hugo generates the HTML based on the layout, theme or template chosen.

This is pretty much a WIP project, one idea would be to use this to fully build the complete website and blog for www.ongres.com.

## Install Hugo

To install hugo follow the instructions found the web: https://gohugo.io/getting-started/installing#linux

This project don't uses Sass/SCSS so the "extended" version is not required.

Check that hugo is installed typing: `hugo version` or `hugo help`

---

## Up and Running

When you clone this repository there are a few things to know:

* The blog posts and other pages are located in `content` folder.
* Images, CSS and other content is located in `static` folder.
* Mostly to follow some structure, blog images are located in `static/assets/blog/`.
* To run the internal server run: `hugo server -D` here the parameter `-D` is to include content marked as draft.
* To compile the site: `hugo` (yes, just that), this generates the site in the `public` folder.

---

## Create a new blog post

Hugo uses something called archetype wich is more o less the "type" of content to create, this project have a basic archetype called "blog" for the purpose of the initial blog setup, this is bassically the front matter included in the new content.

### To create a new post follow this steps:

1. Type: `hugo new blog/this-is-my-cool-blog-post.md`, this creates a new file in the content directory with that name using the blog archetype, avoid using weird characters here.
2. Edit the front matter and tweek it to your needs:
    * `author`: use your real name
    * `tags`: a coma separated list of tags
    * `planet`: is a placeholder in case you wish to publish to the postgresql planet, use your username or word used in the registration of the planet.
    * `date`: if case you wish to change the publication date.
    * `title`: how will be the title of the post.
    * `slug`: is what will be used on the url path, if empty it uses the filename.
    * `draft`: true if this is not ready to be published (it can be build with `-D`)
3. Add your Markdown content for the post (below the +++ front matter).
4. Test your content with `hugo server -D`.

---

## Deploy to production

Once your content is ready remove the `draft: true` and build the site with `hugo`.

### Extra steps

This generates the site html in public folder, there is one extra step to be aware of, due to a bug or limitation from hugo, the taxonomies folders are not "created" inside blog folder.

The taxonomies used are `author`, `tags` and `planet`, so these folders should be moved to blog and combine the content.

### Copy to the ongres-new-web repository

Currently the main web site is in the [ongres-new-web](https://bitbucket.org/ongresinc/ongres-new-web/) repo, once the blog folder is ready, just copy the `public/blog` folder in that repo.

All the images included in `static/assets/blog` should also be copied and if there is some update to the CSS it must be copied too.

## Deploy to staging

Stagging can contain the draft post so it can be build with `-D`, and since the layout uses absoulte urls, the buildURL (`-b`) must be changed:

`hugo -b http://34.235.168.230/ -D`

# Gotchas

Since the idea is to have a single source for the entire web site and blog, care must be taken to check that if the ongres-new-web assets (css, javascript, libs, images) are in sync to avoid problems with the loading of resources.