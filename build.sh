#!/bin/bash

set -e

rm -rf public/

hugo

cp -v public/author/* public/blog/author/
cp -v public/planet/* public/blog/planet/
cp -v public/tags/* public/blog/tags/

sed -i 's,https://www.ongres.com/author/,https://www.ongres.com/blog/author/,g' public/sitemap.xml
sed -i 's,https://www.ongres.com/planet/,https://www.ongres.com/blog/planet/,g' public/sitemap.xml
sed -i 's,https://www.ongres.com/tags/,https://www.ongres.com/blog/tags/,g' public/sitemap.xml


rm -rf public-blog
mkdir -p public-blog
cp -a public/blog/ public-blog/
cp -a public/sitemap.xml public-blog/blog/

rm -rf public/
mv public-blog public
