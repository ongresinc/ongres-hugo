+++
title = "Are PostgreSQL extensions kick-ass... or a liability?"
date = 2018-08-21T15:17:05+02:00
publishDate = 2018-08-24T15:00:05+02:00
author = "Álvaro Hernandez"
draft = true
tags = ["postgresql", "extensions"]
planet = "aht"
+++

Don’t get me wrong. I love Postgres as much as you do --or more--. And I love PostgreSQL’s extensions. Actually, I [pray about them](https://speakerdeck.com/ongres/postgresqls-10-coolest-features?slide=38) being one of PostgreSQL’s best features. But I’m also an incomformist, sometimes questioning conventional wisdom. And I like debate. Hopefully this may start one.

Briefly, for the less initiated: what are extensions? Extensions are a way to package code (functionality) for the database. It could be functions, data types… or changes to the query planner. It’s a quite flexible mechanism. [PostgreSQL’s documentation states that](https://www.postgresql.org/docs/current/static/external-extensions.html):

> “PostgreSQL is designed to be easily extensible. For this reason, extensions loaded into the database can function just like features that are built in”

Some extensions come with PostgreSQL (_contrib_), some can be found on [PGXN](https://pgxn.org/) and others are external (like [cstore_fdw](https://github.com/citusdata/cstore_fdw), for example). Extensions could be written in SQL, or could be compiled C code that needs to be loaded via [shared_preload_libraries](https://postgresqlco.nf/en/doc/param/shared_preload_libraries). This post will focus on the latter.

The story goes like this: I was recently [discussing](https://gitlab.com/gitlab-com/infrastructure/issues/4650) PostgreSQL’s I/O usage on the master database of one of our customers. Most monitoring tools offer I/O metrics, but are in aggregated form. I wanted to drill-down which subcomponents of PostgreSQL were responsible for what amount of read and write I/O: wal writing, checkpointer, queries, autovacuum… you name it. It wasn’t difficult to build a quick solution on top of [iotop](https://github.com/Tomas-M/iotop), AWK and of course a bit of hackish SQL to post-process the data.

Then I was pointed out by {{< tw postgresmen >}} about [bg_mon](https://github.com/CyberDem0n/bg_mon), a tool made by {{< tw cyberdemn >}}, of [Patroni](https://github.com/zalando/patroni)’s fame. It indeed reads the same per-process I/O usage. Even though I don’t know for sure if it presents the information the same way as my awk+SQL scripts, my question was: **can we / should we deploy this tool into our customer’s (GitLab) master instance?**

A PostgreSQL extension can, potentially, access any internals of PostgreSQL, and even alter its behavior. As such, it needs to be totally **trusted code**. Worse: it needs to be of the same quality of PostgreSQL’s source code, because **a crash in the extension’s code will crash your whole database**.

<div class="container">
  <div class="row">
    <div class="col blog-image"><img src="/assets/blog/1-are-postgresql-extensions-kick-ass-or-liability.png"></div>
    <div class="col blog-image"><img src="/assets/blog/2-are-postgresql-extensions-kick-ass-or-liability.png"></div>
    <div class="col blog-image"><img src="/assets/blog/3-are-postgresql-extensions-kick-ass-or-liability.png"></div>
    <div class="col blog-image"><img src="/assets/blog/4-are-postgresql-extensions-kick-ass-or-liability.png"></div>
    <div class="col blog-image"><img src="/assets/blog/5-are-postgresql-extensions-kick-ass-or-liability.png"></div>
    <div class="col blog-image"><img src="/assets/blog/6-are-postgresql-extensions-kick-ass-or-liability.png"></div>
  </div>
</div>

This extension might be a bad example, even if developed by Sasha, as it is just a 46 commits, 2K SLOC, and just 90 lines of code devoted to testing. But speaking in general: may I risk PostgreSQL’s stability with any random extension found on a public git repository? What amount of testing and code audit do I need to perform before loading it?

For a test environment, or small deployments, it might be fine. But for mission-critical, large environments, like GitLab, the answer is typically closer to “No”, “нет”, or simply “Ni de coña”. Actually, cloud providers that offer PGaaS seem to clearly agree with this, as they explicitly whitelist the allowed extensions. And even great extensions like [Timescale](https://www.timescale.com/)’s time-series for PostgreSQL, or *THE* Postgres sharding extension, [Citus](https://www.citusdata.com/), are still not whitelisted on the major cloud providers.

Given cloud adoption trends, this might end up being a liability. If a great extension is not whitelisted by cloud providers, the market for the extension could be significantly reduced (both directly and indirectly).

So, while I believe PostgreSQL extensions are kick-ass, and have served a great purpose to augment core PostgreSQL’s functionality without having to fork PostgreSQL (which proved to be a bad idea), is this the solution we want for the future? What are the alternatives? I envision two possibilities.

The first one would be to turn the extension mechanism a bit upside down, and start exposing much of PostgreSQL’s extensibility mechanisms as public APIs that are consumed outside of the database. If you are thinking “performance, performance, performance”, I’d quickly reply you to not fall intro premature optimization. And that if even that would be the case, I’m willing to trade off some performance for database stability and safeness.

Just as an illustrative example of the kind of interfaces that could be exposed, check [Greenplum’s GPORCA](https://github.com/greenplum-db/gporca). It runs in database space --so it’s not an exact example of what I’m proposing-- but defines a precise interface for input (query and statistics) and output (execution plan) interfaces, and both are… XML! And it’s supposed to be faster than builtin’s query planner! Or think of [Apache Calcite](https://calcite.apache.org/), which is a database… but “without the data”.

The second one would be some way of sandboxing extensions, so that they could not affect the PostgreSQL database. But I guess this is a significant undertaking. Even though not the same, [Dave Cramer](https://twitter.com/dave_cramer)’s [plcontainer](https://github.com/davecramer/plcontainer) seems to go in this direction.

So what do you think, are PostgreSQL extensions kick-ass... or a liability?
